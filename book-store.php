<?php
/**
 * Plugin Name: Book Store Plugin
 * Plugin URI: https://www.aminmoosavi.com/
 * Description: A simple plugin to manage books, authors, and genres for a book store.
 * Version: 1.1
 * Author: Amin Moosavi
 * Author URI: https://www.aminmoosavi.com/
 */

require_once __DIR__ . '/vendor/autoload.php';

use BookStorePlugin\BookStorePlugin;
use BookStorePlugin\PostType;
use BookStorePlugin\Taxonomy;
use BookStorePlugin\MetaBox;

$postType = new PostType();
$taxonomy = new Taxonomy();
$metaBox = new MetaBox();

$bookStorePlugin = new BookStorePlugin($postType, $taxonomy, $metaBox);
$bookStorePlugin->init();
