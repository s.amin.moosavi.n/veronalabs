<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbb49bab669fe92d318913105c300828e
{
    public static $prefixLengthsPsr4 = array (
        'B' => 
        array (
            'BookStorePlugin\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'BookStorePlugin\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbb49bab669fe92d318913105c300828e::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbb49bab669fe92d318913105c300828e::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitbb49bab669fe92d318913105c300828e::$classMap;

        }, null, ClassLoader::class);
    }
}
