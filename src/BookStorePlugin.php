<?php

namespace BookStorePlugin;

class BookStorePlugin
{
    private $postType;
    private $taxonomy;
    private $metaBox;

    public function __construct(PostType $postType, Taxonomy $taxonomy, MetaBox $metaBox)
    {
        $this->postType = $postType;
        $this->taxonomy = $taxonomy;
        $this->metaBox = $metaBox;
    }

    public function init()
    {
        $this->postType->register();
        $this->taxonomy->register();
        $this->metaBox->register();
    }
}
