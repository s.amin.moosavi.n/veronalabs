<?php

namespace BookStorePlugin;

class Taxonomy
{
    public function register()
    {
        add_action('init', array($this, 'register_taxonomies'));
    }

    public function register_taxonomies()
    {
        register_taxonomy('author', 'books', array(
            'label' => 'Author',
            'public' => true,
            'hierarchical' => false,
            'has_archive' => true
        ));

        register_taxonomy('genre', 'books', array(
            'label' => 'Genre',
            'public' => true,
            'hierarchical' => true,
            'has_archive' => true
        ));
    }
}
