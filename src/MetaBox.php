<?php

namespace BookStorePlugin;

class MetaBox
{
    public function register()
    {
        add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
        add_action('save_post', array($this, 'save_book_details'));
        add_filter('the_content',array($this, 'display_meta_boxes_before_content'));
    }

    public function add_meta_boxes()
    {
        add_meta_box('book_store_book_details', 'Book Details', array($this, 'book_details_callback'), 'books', 'normal', 'high');
    }

    public function book_details_callback($post)
    {
        // Nonce field for security
        wp_nonce_field('book_store_save_book_details', 'book_store_book_details_nonce');

        // Retrieve metadata if available
        $author_name = get_post_meta($post->ID, 'author_name', true);
        $isbn = get_post_meta($post->ID, 'isbn', true);
        $price = get_post_meta($post->ID, 'price', true);

        // Display meta box fields
        echo '<p><label for="author_name">Author Name:</label>';
        echo '<input type="text" id="author_name" name="author_name" value="' . esc_attr($author_name) . '" /></p>';

        echo '<p><label for="isbn">ISBN:</label>';
        echo '<input type="text" id="isbn" name="isbn" value="' . esc_attr($isbn) . '" /></p>';

        echo '<p><label for="price">Price:</label>';
        echo '<input type="text" id="price" name="price" value="' . esc_attr($price) . '" /></p>';
    }

    public function save_book_details($post_id)
    {
        // Check if nonce is set and valid
        if (!isset($_POST['book_store_book_details_nonce']) || !wp_verify_nonce($_POST['book_store_book_details_nonce'], 'book_store_save_book_details')) {
            return;
        }

        // Check for autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Check user permissions
        if (!current_user_can('edit_post', $post_id)) {
            return;
        }

        // Sanitize and update metadata
        if (isset($_POST['author_name'])) {
            $author_name = sanitize_text_field($_POST['author_name']);
            update_post_meta($post_id, 'author_name', $author_name);
        }

        if (isset($_POST['isbn'])) {
            $isbn = sanitize_text_field($_POST['isbn']);
            update_post_meta($post_id, 'isbn', $isbn);
        }

        if (isset($_POST['price'])) {
            $price = sanitize_text_field($_POST['price']);
            update_post_meta($post_id, 'price', $price);
        }
    }

    public function display_meta_boxes_before_content($content) {
        // Check if we're inside the main loop in a single post page and the post type is 'books'.
        if (is_singular('books') && in_the_loop() && is_main_query()) {
            // Retrieve metadata if available
            $author_name = get_post_meta(get_the_ID(), 'author_name', true);
            $isbn = get_post_meta(get_the_ID(), 'isbn', true);
            $price = get_post_meta(get_the_ID(), 'price', true);

            // Build the meta box data string
            $meta_box_data = "<div class='book-meta-box'>";
            $meta_box_data .= "<p><strong>Author Name:</strong> " . esc_html($author_name) . "</p>";
            $meta_box_data .= "<p><strong>ISBN:</strong> " . esc_html($isbn) . "</p>";
            $meta_box_data .= "<p><strong>Price:</strong> " . esc_html($price) . "</p>";
            $meta_box_data .= "</div>";

            // Prepend the meta box data to the content
            $content = $meta_box_data . $content;
        }

        return $content;
    }
}
