<?php

namespace BookStorePlugin;

class PostType
{
    public function register()
    {
        add_action('init', array($this, 'register_post_type'));
    }

    public function register_post_type()
    {
        $args = array(
            'public' => true,
            'label' => 'Books',
            'supports' => array('title', 'editor', 'thumbnail', 'custom-fields'),
            'taxonomies' => array('author', 'genre'),
        );
        register_post_type('books', $args);
    }
}
